(async function () {
    //首先检测账户是否登陆，查看用户信息里面的data是否有值，没有则跳转到登陆界面，得到用户信息
    const resp = await API.profile();
    const user = resp.data;
    if (!user) {
        alert('登陆已过期，请重新登陆！');
        location.href = './login.html';
        return;
    }
    //需要的全部dom元素
    const doms = {
        aside: {
            nickname: $('#nickname'),
            loginId: $('#loginId'),
        },
        close: $('.close .icon-close'),
        chatContainer: $('.chat-container'),
        msgContainer: $('.msg-container'),
        txtMsg: $('#txtMsg'),
    }
    //设置用户的姓名等信息
    setUserInfo();

    // 注销事件
    doms.close.onclick = function () {
        API.loginOut();
        location.href = './login.html';
    }

    //加载历史记录
    await localHistory();
    async function localHistory() {
        const resp = await API.getHistory();
        //循环data数组，得到聊天历史记录，添加到页面中
        for (const item of resp.data) {
            addChat(item);
        }
        scrollBottom();
    }

    //发送消息事件
    doms.msgContainer.onsubmit = function(e){
        e.preventDefault();
        sendChat();
    }

    //设置发送聊天到界面
    async function sendChat(){
        const content = doms.txtMsg.value.trim();
        if(!content){
            return; //没有内容直接返回
        }
        addChat({
            from: user.loginId,
            to: null,
            createdAt: Date.now(),
            content,
        });
        doms.txtMsg.value = '';
        scrollBottom();
        const resp = await API.sendChat();
        addChat({
            from: user.loginId,
            to: user.loginId,
            ...resp.data,
        });
        scrollBottom();
    }

    //设置滚动条到最底部的位置
    function scrollBottom(){
        doms.chatContainer.scrollTop = doms.chatContainer.scrollHeight;
    }
    //根据聊天内容添加历史记录
    function addChat(chatInfo) {
        const div = $$$('div');
        div.classList.add('chat-item');
        if (chatInfo.from) {
            div.classList.add('me');
        }
        const img = $$$('img');
        img.className = 'chat-avatar';
        img.src = chatInfo.from ? './asset/avatar.png' : './asset/robot-avatar.jpg';
        const content = $$$('div');
        content.className = 'chat-content';
        content.innerText = chatInfo.content;
        const data = $$$('div');
        data.className = 'chat-date';
        data.innerText = formatDate(chatInfo.createdAt);

        div.appendChild(img);
        div.appendChild(content);
        div.appendChild(data);

        doms.chatContainer.appendChild(div);
    }


    //格式化时间戳
    function formatDate(time) {
        const date = new Date(time);
        const year = date.getFullYear();
        const month = (date.getMonth() + 1).toString().padStart(2, '0');
        const day = date.getDay().toString().padStart(2, '0');
        const hours = date.getHours().toString().padStart(2, '0');
        const minute = date.getMinutes().toString().padStart(2, '0');
        const second = date.getSeconds().toString().padStart(2, '0');
        return `${year}-${month}-${day} ${hours}:${minute}:${second}`;
    }

    //设置用户的姓名以及账号信息到界面
    function setUserInfo() {
        doms.aside.nickname.innerText = user.nickname;
        doms.aside.loginId.innerText = user.loginId;
    }
})();


