var LoginIdValidator = new fieldValidator('txtLoginId', async function (val) {
    if (!val) {
        return '请填写账号';
    }
});

var pwdValidator = new fieldValidator('txtLoginPwd', async function (val) {
    if (!val) {
        return '请填写密码';
    }
});


const form = $('.user-form');
//提交表单时触发，并再次验证四个表单是否都正确
form.onsubmit = async function (e) {
    e.preventDefault();
    const result = await fieldValidator.validate(LoginIdValidator, pwdValidator);
    if(!result){
        return; //验证未通过
    }
    const formData = new FormData(form);
    const data = Object.fromEntries(formData.entries())
    const resp = await API.login(data);
    if(resp.code === 0){
        alert('登陆成功，点击确认进入聊天界面');
        location.href = './index.html';
    }
}