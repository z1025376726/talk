
var LoginIdValidator = new fieldValidator('txtLoginId', async function (val) {
    if (!val) {
        return '请填写账号';
    }
    const result = await API.exists(val);
    if (result.data) {
        return '该账号已存在，请重新设置';
    }
});

var txtNicknameValidator = new fieldValidator('txtNickname', async function (val) {
    if (!val) {
        return '请填写昵称';
    }
});

var pwdValidator = new fieldValidator('txtLoginPwd', async function (val) {
    if (!val) {
        return '请填写密码';
    }
});

var pwdConfirmValidator = new fieldValidator('txtLoginPwdConfirm', async function (val) {
    if (val !== pwdValidator.input.value) {
        return '两次密码不一致，请再次确认';
    }
    if (!val) {
        return '请填写确认密码';
    }
});

const form = $('.user-form');
//提交表单时触发，并再次验证四个表单是否都正确
form.onsubmit = async function (e) {
    e.preventDefault();
    const result = await fieldValidator.validate(LoginIdValidator, txtNicknameValidator, pwdValidator, pwdConfirmValidator);
    if(!result){
        return; //验证未通过
    }
    const formData = new FormData(form);
    const data = Object.fromEntries(formData.entries())
    const resp = await API.reg(data);
    if(resp.code === 0){
        alert('注册成功，点击确认进入登陆界面');
        location.href = './login.html';
    }
}